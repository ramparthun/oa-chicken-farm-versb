#ifndef CHICKENFARM_EGGMAKER_H
#define CHICKENFARM_EGGMAKER_H

#include <random>
#include <thread>

class eggmaker {
private:
    int numberOfEggs;
    int layingInterval;
    bool stop;
public:
    bool isStop() const;
    int getNumberOfEggs() const;
    eggmaker();
    void layEgg();
    void setStop(bool stop);

};


#endif //CHICKENFARM_EGGMAKER_H

#include "qt_farm.h"

qt_farm::qt_farm() {
    id=0;
}

void qt_farm::addChicken() {
    chickenList[id].first=new qt_eggmaker(id);
    chickenList[id].second=new QThread();
    chickenList[id].second->start();
    chickenList[id].first->moveToThread(chickenList[id].second);
    connect(this, &qt_farm::get_signal, chickenList[id].first, &qt_eggmaker::get_slot);
    connect(this,&qt_farm::stop_signal,chickenList[id].first,&qt_eggmaker::stop_slot);
    connect(this,&qt_farm::start_signal,chickenList[id].first,&qt_eggmaker::run);
    connect(chickenList[id].second,&QThread::finished,chickenList[id].first,&qt_farm::deleteLater);
    emit start_signal();
    disconnect(this,&qt_farm::start_signal,chickenList[id].first,&qt_eggmaker::run);
    id++;
}

void qt_farm::stopChicken() {
    emit stop_signal();
}

void qt_farm::deleteChicken() {
    emit stop_signal();
    for(auto it:chickenList){
        it.second->quit();
        it.second->wait();
        delete it.second;
        delete it.first;
    }
    chickenList.clear();
}

void qt_farm::getChicken() {
    emit get_signal();
}

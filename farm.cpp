

#include "farm.h"

void farm::stopChicken() {
    for(auto it:chickenList){
        it.second.first->setStop(true);
    }
}

void farm::addChicken() {
    chickenList[id].first=new eggmaker;
    chickenList[id].second=new std::thread(&eggmaker::layEgg,chickenList[id].first);
    id++;
}

farm::farm() {
    id=0;
}

void farm::deleteChicken() {
    stopChicken();
    for(auto it:chickenList){//case of &it no segfault but the program wont stop
        it.second.second->join();
        delete it.second.second;
        delete it.second.first;
    }
    chickenList.clear();
}

void farm::getChicken() {
    if(chickenList.size()>0){
        for(auto it:chickenList){
            std::cout<<"ID: "<<it.first<<std::endl
                     <<"STOPPED: "<<it.second.first->isStop()<<std::endl
                     <<"NUMBER OF EGGS: "<<it.second.first->getNumberOfEggs()<<std::endl;

        }
    }else{
        std::cout<<"THERE IS NO CHICKEN FOR YOU!"<<std::endl;
    }
}

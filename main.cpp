#include <iostream>
#include <string>
#include <QCoreApplication>
#include "farm.h"
#include "qt_farm.h"
std::string newCommand(){
    std::string command;
    std::cout<<"(commands for std::chicken: create, get, stop, quit(universal))"<<std::endl<<
                "(commands for Qt::chicken: createqt, getqt, stopqt)"<<std::endl<<"next command: "<<std::endl;
    std::cin>>command;
    return command;
};
int main(int argc, char *argv[]){
    QCoreApplication app(argc,argv);
    std::string command;
    bool quit=false;
    farm chickenfarm;
    qt_farm qchickenfarm;
    qchickenfarm.setParent(&app);
    command=newCommand();
    while(not quit){
        if(command=="create"){
            std::cout<<"added chicken"<<std::endl;
            chickenfarm.addChicken();
            command=newCommand();
        }else if(command=="get"){
            std::cout<<"chicken info:"<<std::endl;
            chickenfarm.getChicken();
            command=newCommand();
        }else if(command=="stop"){
            std::cout<<"stopped chicken(s)"<<std::endl;
            chickenfarm.stopChicken();
            command=newCommand();
        }else if(command=="createqt"){
            std::cout<<"added qchicken"<<std::endl;
            qchickenfarm.addChicken();
            command=newCommand();
        }else if(command=="getqt"){
            std::cout<<"qchicken info:"<<std::endl;
            qchickenfarm.getChicken();
            command=newCommand();
        }else if(command=="stopqt"){
            std::cout<<"stopped qchicken(s)"<<std::endl;
            qchickenfarm.stopChicken();
            command=newCommand();
        }else if(command=="quit"){
            std::cout<<"CLUCK CLUCK?"<<std::endl;
            chickenfarm.deleteChicken();
            qchickenfarm.deleteChicken();
            quit=true;
        }else{
            command=newCommand();
        }
   }
    return QCoreApplication::exec();
}
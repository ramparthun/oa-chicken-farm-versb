#ifndef CHICKENFARM_FARM_H
#define CHICKENFARM_FARM_H
#include "eggmaker.h"
#include <thread>
#include <map>
#include <iostream>
class farm {
private:
    int id;
    std::map<int,std::pair<eggmaker*,std::thread*>> chickenList;
public:
    farm();
    void addChicken();
    void stopChicken();
    void deleteChicken();
    void getChicken();
};


#endif //CHICKENFARM_FARM_H

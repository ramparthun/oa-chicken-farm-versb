#ifndef CHICKEN_QT_FARM_H
#define CHICKEN_QT_FARM_H

#include "qt_eggmaker.h"
#include <qmap.h>
#include <QThread>
class qt_farm:public QObject{
    Q_OBJECT
private:
    int id;
    QMap<int,std::pair<qt_eggmaker*,QThread*>> chickenList;
public:
    qt_farm();
    void addChicken();
    void stopChicken();
    void deleteChicken();
    void getChicken();
    signals:
    void stop_signal();
    void get_signal();
    void start_signal();
};


#endif //CHICKEN_QT_FARM_H

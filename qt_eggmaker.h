#ifndef CHICKEN_QT_EGGMAKER_H
#define CHICKEN_QT_EGGMAKER_H
#include <QThread>
#include <QTimer>
#include <random>
#include <iostream>
#include <mutex>
class qt_eggmaker:public QObject{
    Q_OBJECT
private:
    int id;
    int numberOfEggs;
    int layingInterval;
    QTimer *timer;
    std::mutex m;
public:
    int getId() const;
    qt_eggmaker(int x);
    int getNumberOfEggs() const;
    void layEgg();
    void run();
    public slots:
    void stop_slot();
    void get_slot();
};


#endif //CHICKEN_QT_EGGMAKER_H

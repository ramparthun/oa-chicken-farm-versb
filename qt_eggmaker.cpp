#include "qt_eggmaker.h"

qt_eggmaker::qt_eggmaker(int x) {
    id=x;
    std::random_device rd;
    std::mt19937 eng(rd());
    std::uniform_int_distribution<> distr(2, 10);
    layingInterval=distr(eng)*1000;
    numberOfEggs=0;
}

int qt_eggmaker::getNumberOfEggs() const {
    return numberOfEggs;
}

void qt_eggmaker::layEgg() {
    numberOfEggs++;
}

void qt_eggmaker::run() {
    timer=new QTimer();
    timer->setInterval(layingInterval);
    connect(timer, &QTimer::timeout, this, &qt_eggmaker::layEgg);
    timer->start();
}

void qt_eggmaker::stop_slot() {
    timer->stop();
    delete timer;
}

void qt_eggmaker::get_slot() {
    const std::lock_guard<std::mutex> lock(m);
    std::cout<<"ID: "<<getId()<<std::endl;
    std::cout<<"ACTIVE: "<<timer->isActive()<<std::endl;
    std::cout<<"NUMBER OF EGGS: "<<getNumberOfEggs()<<std::endl;
}

int qt_eggmaker::getId() const {
    return id;
}


#include "eggmaker.h"

eggmaker::eggmaker() {
    std::random_device rd;
    std::mt19937 eng(rd());
    std::uniform_int_distribution<> distr(2, 10);
    layingInterval=distr(eng);
    numberOfEggs=0;
    stop=false;
}

int eggmaker::getNumberOfEggs() const {
    return numberOfEggs;
}

void eggmaker::layEgg() {
    while(not stop){
        numberOfEggs++;
        std::this_thread::sleep_for(std::chrono::seconds(layingInterval));
    }
}

void eggmaker::setStop(bool stop) {
    eggmaker::stop = stop;
}

bool eggmaker::isStop() const {
    return stop;
}
